app.controller('ReviewCtrl', ['$scope', '$http', '$uibModal', '$log', '$cookies', '$window', '$sce', function BookingCtrl($scope, $http, $uibModal, $log, $cookies, $window, $sce) {
    checkPageUrl();
    doWorkAfterDOMLoads();
    $scope.cubic_options = {
        cubicInterpolationMode: 'monotone'
    };
    $scope.cubic_colors = ['#fbc02d ', '#00b0ff'];

    function initializeAdpAuthProject() {
        $scope.resetAdpAuthProject()
    }
    $scope.resetAdpAuthProject = function() {
        $scope.adp_auth_password = undefined;
        $scope.adp_auth_keypress_graph = {};
        $scope.adp_auth_mode = ADP_AUTH_MODE.PASSWORD_SETTING_MODE;
        $scope.adp_auth_training_progress = 0;
        $scope.adp_auth_show_key_dynamics = !1;
        $scope.adp_auth_accuracy = "";
        $scope.adp_auth_spline_chart_data = undefined;
        $scope.adp_auth_spline_chart_labels = undefined
    }
    $scope.confirmAdpAuthProjectBtnClick = function() {
        if ($scope.adp_auth_training_progress == 0) {
            $scope.matchPasswords()
        } else if ($scope.adp_auth_training_progress == 100) {
            var accuracy = parseInt($scope.authenticateViaTrainedSpline() * 100) / 100;
            if (!!accuracy) {
                $scope.adp_auth_show_key_dynamics = !0;
                $scope.showAccuracyObtained(accuracy)
            }
        } else {
            $scope.trainSpline()
        }
    }
    $scope.triggerKeyDowns = function(e) {
        $scope.adp_auth_show_key_dynamics = !1;
        var keyPressed = e.key;
        if (!keyPressed.match(ADP_AUTH_ALLOWONLY_REGEX)) {
            e.preventDefault()
        }
        if (keyPressed.length > 1 && keyPressed != "Backspace") {
            e.preventDefault()
        }
        if (keyPressed != "Backspace") {
            $scope.keyDowns[keyPressed] = new Date().getTime();
            Materialize.toast(keyPressed, 500, 'rounded')
        }
    }
    $scope.triggerKeyUps = function(e) {
        var keyLifted = e.key;
        if (keyLifted.length > 1 && keyLifted != "Backspace") {
            e.preventDefault()
        }
        if (keyLifted != "Backspace") {
            $scope.keyUps[keyLifted] = new Date().getTime()
        }
    }
    $scope.hideSplineChart = function() {
        console.log("Hide spline chart");
        $scope.adp_auth_show_key_dynamics = !1;
        $scope.adp_auth_accuracy = ""
    }
    $scope.isPasswordValid = function(password) {
        if (password == undefined || password.length < 5 || password.length > 15) {
            return !1
        }
        return !0
    }
    $scope.arePasswordsSame = function(password1, password2) {
        return password1 === password2
    }
    $scope.matchPasswords = function() {
        var password_entered = document.getElementById("password-entered").value;
        var password_confirmed = document.getElementById("password-confirmed").value;
        if (!$scope.isPasswordValid(password_entered)) {
            Materialize.toast('Enter the password having minimum length 5 and maximum length 15', 2000, 'rounded');
            return
        }
        if (!$scope.isPasswordValid(password_confirmed)) {
            Materialize.toast('Enter the confirmed password having minimum length 5 and maximum length 15', 2000, 'rounded');
            return
        }
        if (!$scope.arePasswordsSame(password_entered, password_confirmed)) {
            Materialize.toast('Passwords are not matching', 2000, 'rounded');
            return
        }
        $scope.adp_auth_password = password_entered;
        $scope.adp_auth_training_progress = 10;
        $scope.adp_auth_mode = ADP_AUTH_MODE.TRAINING_MODE;
        $scope.keyUps = {};
        $scope.keyDowns = {}
    }
    $scope.authenticatePasswordEntered = function(password_entered) {
        if (!$scope.isPasswordValid(password_entered)) {
            Materialize.toast('Enter the password having minimum length 5 and maximum length 15', 2000, 'rounded');
            return !1
        }
        if (!$scope.arePasswordsSame(password_entered, $scope.adp_auth_password)) {
            Materialize.toast('Passwords are not matching', 2000, 'rounded');
            return !1
        }
        return !0
    }
    $scope.trainSpline = function() {
        var password_entered = document.getElementById("training_mode_password").value;
        if (!$scope.authenticatePasswordEntered(password_entered)) {
            $scope.keyUps = {};
            $scope.keyDowns = {};
            document.getElementById("training_mode_password").value = "";
            document.getElementById("training_mode_password").focus();
            return
        }
        if (Object.keys($scope.keyDowns).length != Object.keys($scope.keyUps).length) {
            $scope.keyUps = {};
            $scope.keyDowns = {};
            document.getElementById("training_mode_password").value = "";
            document.getElementById("training_mode_password").focus();
            Materialize.toast('Oops, failed to understand your key dynamics. Please try again', 4000, 'rounded');
            return
        }
        var authGraph = $scope.createAdpAuthGraph(password_entered);
        if ($scope.adp_auth_keypress_graph.Range == undefined) {
            $scope.adp_auth_keypress_graph = authGraph
        } else {
            $scope.trainSplineInternal(authGraph)
        }
        $scope.adp_auth_training_progress = $scope.adp_auth_training_progress + 18;
        $scope.keyUps = {};
        $scope.keyDowns = {};
        document.getElementById("training_mode_password").value = "";
        document.getElementById("training_mode_password").focus()
    }
    $scope.authenticateViaTrainedSpline = function() {
        var password_entered = document.getElementById("training_mode_password").value;
        if (!$scope.authenticatePasswordEntered(password_entered)) {
            $scope.keyUps = {};
            $scope.keyDowns = {};
            document.getElementById("training_mode_password").value = "";
            document.getElementById("training_mode_password").focus();
            return undefined
        }
        if (Object.keys($scope.keyDowns).length != Object.keys($scope.keyUps).length) {
            $scope.keyUps = {};
            $scope.keyDowns = {};
            document.getElementById("training_mode_password").value = "";
            document.getElementById("training_mode_password").focus();
            Materialize.toast('Oops, failed to understand your key dynamics. Please try again', 4000, 'rounded');
            return undefined
        }
        var authGraph = $scope.createAdpAuthGraph(password_entered);
        var graph = $scope.adp_auth_keypress_graph;
        var range = graph.Range;
        var rangeLocal = authGraph.Range;
        var accuracy = $scope.renderSplineChart(graph.X.slice(0), graph.Y.slice(0), range, authGraph.X.slice(0), authGraph.Y.slice(0), rangeLocal, password_entered);
        if (accuracy >= 90) {
            $scope.trainSplineInternal(authGraph)
        }
        $scope.keyUps = {};
        $scope.keyDowns = {};
        document.getElementById("training_mode_password").value = "";
        document.getElementById("training_mode_password").focus();
        return accuracy
    }
    $scope.trainSplineInternal = function(authGraph) {
        var graph = $scope.adp_auth_keypress_graph;
        var x1 = graph.X.slice(0);
        var y1 = graph.Y.slice(0);
        var range = graph.Range;
        var x2 = authGraph.X.slice(0);
        var y2 = authGraph.Y.slice(0);
        var rangeLocal = authGraph.Range;
        var length = x1.length;
        var totalRange = range + rangeLocal;
        x2.push(totalRange);
        y2.push(0);
        var new_x_values = [];
        var new_y_values = [];
        var new_range = 0;
        for (var indx = 0; indx < length; indx++) {
            var new_x_value = x1[indx] + ADP_AUTH_LEARNING_RATE * (x2[indx] - x1[indx]);
            var new_y_value = y1[indx] + ADP_AUTH_LEARNING_RATE * (y2[indx] - y1[indx]);
            new_x_values.push(new_x_value);
            new_y_values.push(new_y_value);
            new_range = new_x_value
        }
        $scope.adp_auth_keypress_graph = {
            "X": new_x_values,
            "Y": new_y_values,
            "Range": new_range
        }
    }
    $scope.createAdpAuthGraph = function(password_entered) {
        var password_entered_length = password_entered.length;
        var x_values = [];
        var y_values = [];
        x_values.push(0)
        y_values.push(0);
        var range = 0;
        for (i = 0; i < password_entered_length; i++) {
            var ch = password_entered[i];
            var interval = $scope.keyUps[ch] - $scope.keyDowns[ch] + CUSHION_TIME;
            var inertia = CUSHION_TIME;
            if (i > 0) {
                inertia = inertia + $scope.keyDowns[ch] - $scope.keyUps[password_entered[i - 1]]
            }
            range = range + inertia;
            x_values.push(range);
            y_values.push(interval)
        }
        var graph = {
            "X": x_values,
            "Y": y_values,
            "Range": range
        };
        return graph
    }
    $scope.renderSplineChart = function(x1, y1, r1, x2, y2, r2, password_entered) {
        var yOne = [];
        var yTwo = [];
        var X = [];
        var length = x1.length;
        var totalRange = r1 + r2;
        x1.push(totalRange);
        x2.push(totalRange);
        y1.push(0);
        y2.push(0);
        var variations = 0;
        for (var indx = 0; indx < length; indx++) {
            X.push(indx);
            yOne.push(x1[indx]);
            yTwo.push(x2[indx]);
            variations = variations + Math.abs(x1[indx] - x2[indx]) / (1 + x1[indx]);
            yOne.push(y1[indx]);
            yTwo.push(y2[indx]);
            variations = variations + Math.abs(y1[indx] - y2[indx]) / (1 + y1[indx])
        }
        variations = variations / (2 * length);
        $scope.adp_auth_spline_chart_data = [yOne, yTwo];
        $scope.adp_auth_spline_chart_labels = X;
        return 100 * (1.0 - variations)
    }
    $scope.relu = function(val) {
        if (!val || val < 0) {
            return 0
        }
        return val
    }
    $scope.onAdpAuthDemoBtnClick = function() {
        $('#adaptive_authentication-modal').modal('open');
        $scope.resetAdpAuthProject()
    }
    $scope.showAccuracyObtained = function(accuracy) {
        $scope.adp_auth_accuracy = "Authentication Meter: <b>" + accuracy + " %</b><br>Meter shows the likelihood of the person being the actual person whose password is being authenticated.</br></br><b>Click on the graph to try out again!";
        $scope.adp_auth_accuracy = $sce.trustAsHtml($scope.adp_auth_accuracy)
    }
    $scope.$watch('adp_auth_training_progress', function() {
        if ($scope.adp_auth_training_progress == 100) {
            $scope.adp_auth_mode = ADP_AUTH_MODE.AUTHENTICATION_MODE;
            $scope.adp_auth_header_msg_text = "<b>" + $scope.adp_auth_mode + "</b>";
            $scope.adp_auth_header_msg_html = $sce.trustAsHtml($scope.adp_auth_header_msg_text)
        } else if ($scope.adp_auth_training_progress >= 10) {
            $scope.adp_auth_mode = ADP_AUTH_MODE.TRAINING_MODE;
            $scope.adp_auth_header_msg_text = "<b>" + $scope.adp_auth_mode + " ( " + $scope.adp_auth_training_progress + "% ) " + "</b>";
            $scope.adp_auth_header_msg_html = $sce.trustAsHtml($scope.adp_auth_header_msg_text);
            Materialize.toast("Training Completed: " + $scope.adp_auth_training_progress + " %", 1000, 'rounded')
        } else {
            $scope.resetAdpAuthProject();
            $scope.adp_auth_header_msg_text = "<b>" + $scope.adp_auth_mode + "</b>";
            $scope.adp_auth_header_msg_html = $sce.trustAsHtml($scope.adp_auth_header_msg_text)
        }
    });

    function initializeLed7Project() {
        reset7SegmentDisplay()
    }
    $scope.on7SegmentLedClicked = function(ledId) {
        ledId = parseInt(ledId);
        if ($scope.led_classes[ledId] == LED7_CLASSES_MAP[!1]) {
            $scope.led_classes[ledId] = LED7_CLASSES_MAP[!0]
        } else {
            $scope.led_classes[ledId] = LED7_CLASSES_MAP[!1]
        }
        $scope.led7_user_status = LED7_USER_STATUSES_MAP.ACTIVE_SELECTION
    }
    $scope.onLed7DemoBtnClick = function(type) {
        $('#led7-modal').modal('open');
        $scope.led7_type = type;
        reset7SegmentDisplay()
    }
    $scope.onLed7PredictBtnClick = function() {
        led7Predict()
    }

    function reset7SegmentDisplay() {
        $scope.led7_user_status = LED7_USER_STATUSES_MAP.INACTIVE
    }

    function led7Predict() {
        $scope.led7_user_status = LED7_USER_STATUSES_MAP.ACTIVE_PREDICTION;
        var state_string = "";
        for (index = 0; index < $scope.led_classes.length; index++) {
            if ($scope.led_classes[index] == LED7_CLASSES_MAP[!0]) {
                state_string += "1"
            } else {
                state_string += "0"
            }
        }
        $http({
            method: 'GET',
            url: BASE_URL + LED7_PROJECT_NAME + '/predict',
            headers: {
                'Content-Type': 'application/json'
            },
            params: {
                segment_state: state_string,
                type: $scope.led7_type
            }
        }).then(function(response) {
            if (response.data.status && !response.data.error) {
                var responseData = JSON.parse(response.data.response);
                $scope.led7_labels = responseData.labels;
                $scope.led7_probability = responseData.probabilities;
                $scope.led7_prediction = responseData.prediction;
                $scope.led7_user_status = LED7_USER_STATUSES_MAP.SUCCESS
            } else {
                Materialize.toast('Oops, Something went wrong!', 2000, 'rounded');
                reset7SegmentDisplay()
            }
        }, function(response) {
            Materialize.toast('Oops, Something went wrong!', 2000, 'rounded');
            reset7SegmentDisplay()
        })
    };
    $scope.$watch('led7_user_status', function() {
        if ($scope.led7_user_status == LED7_USER_STATUSES_MAP.INACTIVE) {
            $scope.led_classes = ["off", "off", "off", "off", "off", "off", "off"];
            $scope.led7_labels = [];
            $scope.led7_probability = [];
            $scope.led7_prediction = "";
            $scope.led7_header_msg_text = "<b>Click on the segments of 7-Segment DISPLAY to toggle them.</b>";
            $scope.led7_header_msg_html = $sce.trustAsHtml($scope.led7_header_msg_text);
            $scope.led7_predict_btn_visibility = !1;
            $scope.led7_prediction_process = !1
        } else if ($scope.led7_user_status == LED7_USER_STATUSES_MAP.ACTIVE_SELECTION) {
            $scope.led7_labels = [];
            $scope.led7_probability = [];
            $scope.led7_prediction = "";
            $scope.led7_header_msg_text = "<b>Click on the segments of 7-Segment DISPLAY to toggle them.</b>";
            $scope.led7_header_msg_html = $sce.trustAsHtml($scope.led7_header_msg_text);
            $scope.led7_predict_btn_visibility = !0;
            $scope.led7_prediction_process = !1
        } else if ($scope.led7_user_status == LED7_USER_STATUSES_MAP.ACTIVE_PREDICTION) {
            $scope.led7_header_msg_text = "";
            $scope.led7_header_msg_html = $sce.trustAsHtml($scope.led7_header_msg_text);
            $scope.led7_predict_btn_visibility = !1;
            $scope.led7_prediction_process = !0
        } else if ($scope.led7_user_status == LED7_USER_STATUSES_MAP.SUCCESS) {
            $scope.led7_header_msg_text = "<b>IT'S A BIRD? . . . IT'S A PLANE? . . . NOPE! . . . . . IT'S " + $scope.led7_prediction + "</b>";
            $scope.led7_header_msg_html = $sce.trustAsHtml($scope.led7_header_msg_text);
            $scope.led7_predict_btn_visibility = !1;
            $scope.led7_prediction_process = !1;
            Materialize.toast("Model predicted chance % of each digit shown in pie chart! ML <3", 4000, 'rounded')
        } else reset7SegmentDisplay()
    });

    function checkVisits() {
        var visits = $cookies.get("visits");
        console.log(visits)
        if (!visits) {
            visits = "0"
        }
        var visits_int = parseInt(visits);
        $cookies.put("visits", visits_int + 1);
        return visits_int
    }

    function doWorkAfterDOMLoads() {
        angular.element(function() {
            $('.collapsible').collapsible();
            $('.modal').modal();
            checkVisits();
            initializeLed7Project();
            initializeAdpAuthProject();
            setTimeout(function() {
                $('.collapsible').collapsible();
                $('.modal').modal();
                console.log("Initialized DOM")
            }, 2000)
        })
    }

    function checkPageUrl() {
        var loc = $window.location.href;
        console.log("Location: " + loc)
    }
}]);

var BASE_URL = "http://api.yogeshjadhav.website/"
var LED7_PROJECT_NAME = "led7";
var LED7_CLASSES_MAP = {
    true: "on",
    false: "off"
};
var LED7_USER_STATUSES_MAP = {
    "INACTIVE": "inactive",
    "ACTIVE_SELECTION": "active_selection",
    "ACTIVE_PREDICTION": "active_prediction",
    "SUCCESS": "success"
};
var ADP_AUTH_PASSWORD_LIMIT = 15
var ADP_AUTH_MODE = {
    "TRAINING_MODE": "#STEP 2: TRAINING MODE ( The model is being trained by your keystroke dynamics as you type your password )",
    "AUTHENTICATION_MODE": "#STEP 3: AUTHENTICATION MODE ( Time to test the model. You can also let someone else type the same password and witness the authentication meter drastically dip!! ) ",
    "PASSWORD_SETTING_MODE": "#STEP 1: SETTING PASSWORD ( Enter and confirm your desired password to move onto the training mode )"
};
var ADP_AUTH_ALLOWONLY_REGEX = /^[0-9a-zA-Z]+$/;
var ADP_AUTH_LEARNING_RATE = 0.075;
var CUSHION_TIME = 10
