app.filter('offset', function () {
    return function (input, start) {
        if (input == null)return;
        return input.slice(start - 20, start);
    };
});
app.filter('highlightWord', function () {
    return function (text, selectedWord) {
        var isBlank = (!selectedWord || /^\s*$/.test(selectedWord));
        if (selectedWord && !isBlank) {
            var pattern = new RegExp(selectedWord, "gi");
            return text.replace(pattern, '<span class="highlighted">' + selectedWord + '</span>');
        }
        else {
            return text;
        }
    };
});

